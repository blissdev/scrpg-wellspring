export type EntityType =
    "players"
    | "minions"
    | "villians"
    | "lieutenants"
    | "challenges"
    | "environments";

export type Entity = {
    name: string,
}

export type Challenge = Entity & {
    subtasks: Task[]
};

export type Task = Entity & {
    completed: boolean
};

export type TappableEntity = Entity & {
    tapped: boolean
};

export type ReactableEntity = Entity & TappableEntity & {
    reactionUsed: boolean,
};

export type Environment = TappableEntity;
export type Player = ReactableEntity;
export type Villain = ReactableEntity;
export type Lieutenant = TappableEntity;
export type Minion = TappableEntity;
