import {
    Entity,
    Challenge,
    Environment,
    Player,
    Villain,
    Lieutenant,
    Minion
} from './entity';

type Progress = {
    segments: { [key: string]: number },
    current: number
};

export type Scene = {
    progress?: Progress
    challenges: Challenge[],
    environments:  Environment[],
    players: Player[]
    villains: Villain[],
    lieutenants: Lieutenant[],
    minions: Minion[],
}

export type EntityReference = {
    entityType: string,
    index: number
};

export function getSceneEntity(scene: Scene, entityType: string, index: number): Entity | undefined {
    if (["players", "minions", "villains", "lieutenants", "environments", "challenges"].includes(entityType)) {
        return (scene[entityType as keyof Scene] as Entity[])[index];
    }
    return;
}
