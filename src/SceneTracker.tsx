import { Scene } from './model/scene';

function ProgressStep(props: {
    index: number,
    color: string,
    current: number
}) {
    const classes = ["progress"];
    classes.push(props.color);

    if (props.index + 1 <= props.current) {
        classes.push("consumed");
    }

    return (
        <div className={classes.join(" ")}></div>
    );
}

type SceneTrackerProps = {
    scene: Scene,
    dispatch: any,
    setEditingScene: (value: boolean) => void,
};

function SceneTracker(props: SceneTrackerProps) {
    if (!props.scene.progress) return null;

    const progress = props.scene.progress;

    return (
        <div className="card scene" onClick={() => props.setEditingScene(true)}>
            <b>Scene Tracker</b>
            <div className="progress-container">
                {[...Array(progress.segments.g)].map((_, i) => {
                    return (
                        <ProgressStep
                            color="green"
                            index={i}
                            key={i}
                            current={progress.current}
                        />
                    );
                })}
                {[...Array(progress.segments.y)].map((_, i) => {
                    return (
                        <ProgressStep
                            color="yellow"
                            index={i+progress.segments.g}
                            key={i+progress.segments.g}
                            current={progress.current}
                        />
                    );
                })}
                {[...Array(progress.segments.r)].map((_, i) => {
                    return (
                        <ProgressStep
                            color="red"
                            index={i+progress.segments.g+progress.segments.y}
                            key={i+progress.segments.g+progress.segments.y}
                            current={progress.current}
                        />
                    );
                })}
            </div>
            <button onClick={(e) => { e.stopPropagation(); props.dispatch({ type: 'DEC_PROGRESS' })}}>Previous</button>
            <button onClick={(e) => { e.stopPropagation(); props.dispatch({ type: 'INC_PROGRESS' })}}>Next</button>
        </div>
    );
}

export default SceneTracker;
