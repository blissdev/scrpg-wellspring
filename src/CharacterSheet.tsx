import React from 'react';

function CharacterSheet() {
  return (
    <div className="character-sheet">
        <header>
            <h1>Sidereal <span>the Astromancer</span></h1>
            <dl>
                <dt>Health</dt>
                <dd>30</dd>
                <dt>Dice</dt>
                <dd>
                    green: d6
                    yellow: d6
                    red: d12
                </dd>
            </dl>
        </header>
        <section id="powers">
            <h2>Powers</h2>
            <div className="powers">
                <div className="power">
                    <div>Absorption</div>
                    <div>d12</div>
                </div>
                <div className="power">
                    <div>Awareness</div>
                    <div>d6</div>
                </div>
                <div className="power">
                    <div>Vitality</div>
                    <div>d6</div>
                </div>
                <div className="power">
                    <div>Cosmic</div>
                    <div>d12</div>
                </div>
                <div className="power">
                    <div>Teleportation</div>
                    <div>d6</div>
                </div>
            </div>
        </section>
        <section id="qualities">
            <h2>Qualities</h2>
            <div className="powers">
                <div className="power">
                    <div>Science</div>
                    <div>d10</div>
                </div>
                <div className="power">
                    <div>Imposing</div>
                    <div>d10</div>
                </div>
                <div className="power">
                    <div>Optimistically Weathered</div>
                    <div>d8</div>
                </div>
            </div>
        </section>
        <section id="abilities">
            <h2>Abilities</h2>
            <div className="ability zone-green">
                <div className="basic-action">Defend</div>
                <div className="name">Ambush Awareness</div>
                <div className="type">Reaction</div>
                <div className="description">
                    If you haven’t yet acted in an action scene, you may Defend against an Attack by rolling your single <span className="power">Awareness</span> die.
                </div>
            </div>
            <div className="ability zone-green">
                <div className="basic-action">Attack</div>
                <div className="name">Energy Jaunt</div>
                <div className="type">Action</div>
                <div className="description">
                   Attack multiple targets using <span className="power">Vitality</span>, applying your Min die against each.
                </div>
            </div>
            <div className="ability zone-green">
                <div className="basic-action">Attack/Hinder</div>
                <div className="name">Subdue</div>
                <div className="type">Action</div>
                <div className="description">
                    Attack using <span className="power">Absorption</span>. Hinder the same target using your Min die.
                </div>
            </div>
            <div className="ability zone-yellow">
                <div className="basic-action">Attack</div>
                <div className="name">Reflexive Burst</div>
                <div className="type">Reaction</div>
                <div className="description">
                    When your personal zone changes, Attack all close enemy targets by rolling your single <span className="power">Absorption</span> die. 
                </div>
            </div>
            <div className="ability zone-yellow">
                <div className="basic-action">Attack/Hinder</div>
                <div className="name">Inflict</div>
                <div className="type">Action</div>
                <div className="description">
                    Attack using <span className="power">Vitality</span>. Hinder that same target using your Min die. 
                </div>
            </div>
            <div className="ability zone-yellow">
                <div className="basic-action">Attack</div>
                <div className="name">Living Bomb</div>
                <div className="type">Action</div>
                <div className="description">Destroy one d6 or d8 minion. Roll that minion's die as an Attack against another target.</div>
            </div>
            <div className="ability zone-red">
                <div className="basic-action">Attack</div>
                <div className="name">Charged Up Blast</div>
                <div className="type">Action</div>
                <div className="description">
                    Attack using <span className="power">Cosmic</span> and at least bonus.
                    Use your Max+Mid+Min dice. Destroy all of your bonuses, adding each of them to this Attack first, even if they are exclusive. 
                </div>
            </div>
            <div className="ability zone-red">
                <div className="basic-action">Boost</div>
                <div className="name">Lead By Example</div>
                <div className="type">Action</div>
                <div className="description">
                    Make a basic action using <span className="power">Optimistically Weathered</span>. Use your Max die. All other heroes who take the same basic action on their turn against the same target receive a Boost from your Mid+Min dice.
                </div>
            </div>
            <div className="ability zone-red">
                <div className="basic-action">Boost</div>
                <div className="name">Critical Eye</div>
                <div className="type">Action</div>
                <div className="description">
                    Select a target. Boost using <span className="power">Science</span>.
                    Use your Max+Mid+Min dice.That bonus must be used against that target before the end of your next turn, or it is wasted.
                </div>
            </div>
        </section>
    </div>
  );
}

export default CharacterSheet;
