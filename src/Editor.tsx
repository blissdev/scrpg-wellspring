import { EntityReference, Scene, getSceneEntity } from './model/scene';

type EditorProps = {
    editingEntity: EntityReference,
    setEditingEntity: (value: EntityReference | undefined) => void,
    scene: Scene,
    dispatch: any,
};

function Editor({ scene, editingEntity, setEditingEntity, dispatch }: EditorProps) {
    const entity = getSceneEntity(scene, editingEntity.entityType, editingEntity.index);

    return (
        <div
            className="modal"
            onClick={(e) =>
                (e.target as Element).classList.contains('modal') && setEditingEntity(undefined)
            }
        >
            <div className="modal-body">
                <input
                    type="text"
                    name="name"
                    value={entity!.name}
                    onChange={(e) =>
                        dispatch({ type: 'UPDATE_NAME', reference: editingEntity, value: e.target.value })
                    }
                />
                <button onClick={() => {
                    setEditingEntity(undefined);
                    dispatch({ type: 'REMOVE_ENTITY', reference: editingEntity});
                }}>
                    Remove
                </button>
            </div>
        </div>
    );
}

export default Editor;
