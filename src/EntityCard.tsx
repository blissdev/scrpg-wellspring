import { Entity, ReactableEntity, TappableEntity } from './model/entity';
import { EntityReference, Scene } from './model/scene';

const entityClassMap = {
    players: "player",
    villains: "villain",
    minions: "minion",
    lieutenants: "lieutenant",
    challenges: "challenge",
    environments: "environment",
    // Figure out how to remove
    progress: "progress",
};

const tappableTypes = ["players", "villains", "minions", "lieutenants", "challenges", "environments"];
const reactableTypes = ["players", "villains"];

type CardProps = {
    scene: Scene,
    entity: Entity,
    reference: EntityReference,
    setEditingEntity: (value: EntityReference | undefined) => void,
    dispatch: any
}

function EntityCard(props: CardProps) {
    const classes = ["card"];

    const entity = props.entity;

    // Use Pick or similar?
    classes.push(entityClassMap[props.reference.entityType as keyof Scene]);

    if (tappableTypes.includes(props.reference.entityType)) {
        classes.push((entity as TappableEntity).tapped ? "tapped" : "not-tapped");
    }

    if (reactableTypes.includes(props.reference.entityType)) {
        classes.push((entity as ReactableEntity).reactionUsed ? "has-reacted" : "no-reaction");
    }

    return (
        <div className={classes.join(' ')} onClick={() => props.setEditingEntity(props.reference)}>
            <b>{props.entity!.name}</b>
            {tappableTypes.includes(props.reference.entityType) &&
                <button onClick={(e) => {
                    props.dispatch({
                        type: 'FLIP',
                        reference: props.reference,
                        value: !(props.entity as TappableEntity).tapped
                    });
                    e.stopPropagation();
                }}>
                    {(props.entity as TappableEntity).tapped ? "Untap" : "Tap"}
                </button>
            }
            {reactableTypes.includes(props.reference.entityType) &&
                <button onClick={(e) => {
                    props.dispatch({
                        type: 'REACT',
                        reference: props.reference,
                        value: !(props.entity as ReactableEntity).reactionUsed
                    });
                    e.stopPropagation();
                }}>
                    {!(props.entity as ReactableEntity).reactionUsed ? "React" : "UnReact"}
                </button>
            }
        </div>
    );
}

type ListProps = {
    entityType: string,
    scene: Scene,
    setEditingEntity: (value: EntityReference | undefined) => void,
    dispatch: any
};

function EntityList(props: ListProps) {
    const entitySet = props.scene[props.entityType as keyof Scene] as Entity[];
    return (
        <div className={props.entityType}>
            {entitySet.map((entity, i) => {
                return (
                    <EntityCard
                        scene={props.scene}
                        entity={entity}
                        reference={{ entityType: props.entityType, index: i }} 
                        key={i}
                        setEditingEntity={props.setEditingEntity}
                        dispatch={props.dispatch}
                    />
                );
            })}
        </div> 
    );
}

export { EntityCard, EntityList };
