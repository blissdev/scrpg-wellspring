import Editor from './Editor';
import { EntityList } from './EntityCard';
import SceneTracker from './SceneTracker';
import { useReducer, useState } from 'react';
import { current, produce } from 'immer';
import { Entity, ReactableEntity, TappableEntity } from './model/entity';
import { EntityReference, Scene, getSceneEntity } from './model/scene';
import './App.css';

const initialState: Scene = {
    players: [
        { name: "A.3.G.I.S.", tapped: false, reactionUsed: false },
        { name: "Baroness Blade", tapped: false, reactionUsed: false },
        { name: "Faceless", tapped: false, reactionUsed: false },
        { name: "Shimmering Lotus", tapped: false, reactionUsed: false },
        { name: "The Smith", tapped: false, reactionUsed: false },
        { name: "Pica Chew", tapped: false, reactionUsed: false },
        { name: "Sidereal", tapped: false, reactionUsed: false },
    ],
    progress: {
        segments: { g: 2, y: 4, r: 2 },
        current: 3,
    },
    challenges: [
        {
            name: "Fire in the Gallery",
            subtasks: [
                { name: "Put out the fires", completed: true },
                { name: "Clear the smoke", completed: false },
            ],
        }
    ],
    environments: [{
        name: "Default Environment",
        tapped: false,
    }],
    villains: [],
    lieutenants: [
        { name: "\"Revenant\"", tapped: false },
    ],
    minions: [
        { name: "Snipers", tapped: false },
        { name: "Paramilitary Troopers", tapped: false },
    ],
};

type DispatchAction = {
    type: string,
    reference?: EntityReference,
    entityType?: string,
    value?: any
    segment?: string,
    segmentAction?: string,
};

function getInitState(entityType: string): Entity | TappableEntity | ReactableEntity {
    switch(entityType) {
        case "villains":
            return {
                name: "New Villain",
                tapped: false,
                reactionUsed: false
            };
        case "lieutenants":
            return {
                name: "New Lieutenant",
                tapped: false,
            };
        case "minions":
            return {
                name: "New Minion",
                tapped: false,
            };
        case "players":
            return {
                name: "New Player",
                tapped: false,
                reactionUsed: false
            };
        default:
            return {
                name: "Default",
            };
    };
}

function reducer(state: Scene, action: DispatchAction): Scene {
    switch (action.type) {
        case 'ADD_PLAYER':
            return produce(state, draft => {
                draft.players.push({ name: "New Player", tapped: false, reactionUsed: false });
            });

        case 'ADD_ENTITY':
            return produce(state, draft => {
                if(action.entityType) {
                    const entitySet = draft[action.entityType as keyof Scene] as Entity[];
                    entitySet.push(getInitState(action.entityType));
                }
            });

        case 'REMOVE_ENTITY':
            return produce(state, draft => {
                if (action.reference) {
                    const entitySet = draft[action.reference.entityType as keyof Scene] as Entity[];
                    entitySet.splice(action.reference.index, 1)
                }
            });

        case 'FLIP':
            return produce(state, draft => {
                if (action.reference) {
                    const idx = action.reference.index;
                    (getSceneEntity(draft, action.reference.entityType, idx)! as TappableEntity).tapped = action.value;
                }
            });

        case 'REACT':
            return produce(state, draft => {
                if (action.reference) {
                    const idx = action.reference.index;
                    (getSceneEntity(draft, action.reference.entityType, idx)! as ReactableEntity).reactionUsed = action.value;
                }
            });

        case 'UPDATE_NAME':
            return produce(state, draft => {
                if (action.reference) {
                    const idx = action.reference.index;
                    (getSceneEntity(draft, action.reference.entityType, idx)! as TappableEntity).name = action.value;
                }
            });

        case 'INC_PROGRESS':
            return produce(state, draft => {
                const currentProgress = current(draft).progress;
                if (!currentProgress) return;
                const total =
                    currentProgress.segments.g
                    + currentProgress.segments.y
                    + currentProgress.segments.r;
                if(currentProgress && currentProgress.current < total) {
                    draft.progress!.current = currentProgress.current + 1;
                }
            });

        case 'DEC_PROGRESS':
            return produce(state, draft => {
                const currentProgress = current(draft).progress;
                if(currentProgress && currentProgress.current > 0) {
                    draft.progress!.current = currentProgress.current - 1;
                }
            });

        case 'UPDATE_SEGMENT_COUNT':
            return produce(state, draft => {
                const segment = action.segment!;
                const currentSegmentCount = current(draft).progress!.segments[segment]
                draft.progress!.segments[segment] = action.segmentAction! === "INC"
                    ? currentSegmentCount + 1
                    : currentSegmentCount - 1;
            });

        default:
            return state;
    }
}

function App() {
    const [state, dispatch] = useReducer(reducer, initialState);
    const [editingEntity, setEditingEntity] = useState<EntityReference | undefined>(undefined);
    const [editingScene, setEditingScene] = useState<boolean>(false);

    return (
        <div className="App">
            <div className="controls">
                <button>Add Location</button>
                <button>Add Challenge</button>
                <button onClick={() => dispatch({ type: 'ADD_ENTITY', entityType: "players" })}>Add Player</button>
                <button onClick={() => dispatch({ type: 'ADD_ENTITY', entityType: "villains" })}>Add Villain</button>
                <button onClick={() => dispatch({ type: 'ADD_ENTITY', entityType: "lieutenants" })}>Add Lieutenant</button>
                <button onClick={() => dispatch({ type: 'ADD_ENTITY', entityType: "minions" })}>Add Minion</button>
            </div>
            <div className="GameStateView">
                <EntityList
                    entityType="players"
                    setEditingEntity={setEditingEntity}
                    dispatch={dispatch}
                    scene={state}
                />
                <div className="environment-container">
                    <EntityList
                        entityType="environments"
                        setEditingEntity={setEditingEntity}
                        dispatch={dispatch}
                        scene={state}
                    />
                    <EntityList
                        entityType="challenges"
                        setEditingEntity={setEditingEntity}
                        dispatch={dispatch}
                        scene={state}
                    />
                    {state.progress &&
                        <SceneTracker
                            scene={state}
                            dispatch={dispatch}
                            setEditingScene={setEditingScene}
                        />}
                </div>
                <div className="enemies">
                    <EntityList
                        entityType="villains"
                        setEditingEntity={setEditingEntity}
                        dispatch={dispatch}
                        scene={state}
                    />
                    <EntityList
                        entityType="lieutenants"
                        setEditingEntity={setEditingEntity}
                        dispatch={dispatch}
                        scene={state}
                    />
                    <EntityList
                        entityType="minions"
                        setEditingEntity={setEditingEntity}
                        dispatch={dispatch}
                        scene={state}
                    />
                </div> 
            </div>
            {editingEntity && (
                <Editor
                    editingEntity={editingEntity}
                    setEditingEntity={setEditingEntity}
                    scene={state}
                    dispatch={dispatch}
                />
            )}
            {editingScene && (
                <div
                    className="modal"
                    onClick={(e) =>
                        (e.target as Element).classList.contains('modal') && setEditingScene(false)
                    }
                >
                    <div className="modal-body">
                        {["g", "y", "r"].map((color) => {
                            const count = state.progress!.segments[color];
                            return (
                                <div className="row" key={color}>
                                    {count > 0 && (
                                        <button onClick={() => dispatch({ type: 'UPDATE_SEGMENT_COUNT', segmentAction: "DEC", segment: color })}>-</button>
                                    )}
                                    <span>{(color === "g" && "Green") || (color === "y" && "Yellow") || "Red"} ({count})</span>
                                    <button onClick={() => dispatch({ type: 'UPDATE_SEGMENT_COUNT', segmentAction: "INC", segment: color })}>+</button>
                                </div>
                            );
                        })}
                    </div>
                </div>
            )}
        </div>
    );
}

export default App;
